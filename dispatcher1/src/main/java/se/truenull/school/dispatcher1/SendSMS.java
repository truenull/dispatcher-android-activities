package se.truenull.school.dispatcher1;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

public class SendSMS extends ActionBarActivity {
    static final int PICK_LOCATION_REQUEST = 1;
    String selected_message = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final String def = getString(R.string.location_usual);
        String res = "";
        if (requestCode == PICK_LOCATION_REQUEST) {
            if (resultCode == RESULT_OK) {
                res = data.getStringExtra(getString(R.string.index_location));
            }
        }

        selected_message = getString(R.string.location_message) + " "
                + ((res == null) ? def : res) + ".";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_sms);

        selected_message = getString(R.string.default_message);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.send_sm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickRadioButton(View view) {
        RadioButton rb = (RadioButton) view;

        selected_message = (String) rb.getText();
        if(rb.getId() == R.id.radio_location)
        {
            selected_message += " the usual place.";
        }
    }

    public void onClickButton(View view) {
        switch (view.getId()) {
            case R.id.recipient_bill:
                sendSMS("00072456");
                break;
            case R.id.recipient_borje:
                sendSMS("00032456");
                break;
            case R.id.recipient_orjan:
                sendSMS("00012456");
                break;
        }
    }

    private void sendSMS(String number) {
        Uri uri = Uri.parse("sms:" + number);
        Intent send = new Intent(Intent.ACTION_SENDTO, uri);
        send.putExtra("sms_body", this.selected_message);
        startActivity(send);
    }

    public void onTranslateClick(View view) {
        Uri.Builder url = Uri.parse("http://translate.google.com/m/translate?sl=en&tl=es").buildUpon();
        url.appendQueryParameter("q", selected_message);
        Intent web = new Intent(Intent.ACTION_VIEW, url.build());
        startActivity(web);
    }

    public void onClickLocation(View view) {
        Intent getLoc = new Intent(SendSMS.this, LocationActivity.class);
        startActivityForResult(getLoc, this.PICK_LOCATION_REQUEST);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_send_sm, container, false);
            return rootView;
        }
    }

}
